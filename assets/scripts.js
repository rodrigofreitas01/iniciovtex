/*dp - Prefixo usado para identificar a loja*/

const codeEplusAlpha = {
    menuPrincial: function() {
        var settings = {
            "url": "/api/catalog_system/pub/category/tree/3/",
            "method": "GET",
            "headers": {
                "content-type": "application/json"
            }
        }
        $.ajax(settings).done(function(dataMenu) {
            let menu = $('.menu-principal');
            let eplusMenu = dataMenu;
            $.each(eplusMenu, function(i, field) {
                menu.append('<li class="itemMenu cat' + field.id + ' cat-index-' + i + '"><a href="' + field.url + '" title="' + field.name + '">' + field.name + '</a><ul class="subCategoria"><div class="container-center"></div></ul></li>');
                $(eplusMenu[i].children).each(function(ii, el) {
                    let subCatId = eplusMenu[i].children[ii].id;
                    let subCatNome = eplusMenu[i].children[ii].name;
                    let subCatUrl = eplusMenu[i].children[ii].url;
                    $('.cat-index-' + i).find('.container-center').append('<li id="' + subCatId + '" class="subItem"><a href="' + subCatUrl + '" class="link">' + subCatNome + '</a> <ul> </ul></li>');
                    $(eplusMenu[i].children[ii].children).each(function(iii, el) {
                        let subCatSubId = eplusMenu[i].children[ii].children[iii].id;
                        let subCatSubNome = eplusMenu[i].children[ii].children[iii].name;
                        let subCatSubUrl = eplusMenu[i].children[ii].children[iii].url;
                        $('.cat-index-' + i).find('#' + subCatId).find('ul').append('<li id="' + subCatSubId + '" class="subItemSub"><a href="' + subCatSubUrl + '" class="link">' + subCatSubNome + '</a></li>')
                    })
                })
            })
        }).done(function() {
            $(".menu-principal > li > ul").each(function(i) {
                let subMenu = $(this).find(".subItem").length;
                if (subMenu < 1) {
                    $(this).remove()
                } else {
                    $(this).closest("li").addClass("subMenuTrue")
                }
            });
            $(".menu-principal > li > ul .subItem").each(function(i) {
                if (screen > 769) {
                    thisLinksSub = $(this).find('.subItemSub');
                    for (let i = 0; i < thisLinksSub.length; i += 12) {
                        thisLinksSub.slice(i, i + 12).wrapAll("<span class='menuColuna'></span>")
                    }
                }
            });
        })
    },
    qtdCart: function() {
        vtexjs.checkout.getOrderForm().done(function(e) {
            let qtdCart = e.items.length;
            if (screen < 1000) {
                $(".header .carrinho .qtd-cart").text(qtdCart)
            } else {
                $(".header .carrinho .qtd-cart").prepend(qtdCart)
            }
        })
    },
    verificaLogado: function() {
        vtexjs.checkout.getOrderForm().done(function(orderForm) {
            let logado = orderForm.loggedIn;
            $(".header .saudacao-topo").prepend("<span class='saudacao-icon'> </span>");
            if (logado) {
                $(".header .saudacao-topo").append('<ul>' + '<li><a href="/account">Minha Conta</a></li>' + '<li><a href="/account/orders">Meus Pedidos</a></li>' + '<li><a href="/no-cache/user/logout">Sair</a></li>');
                setTimeout(function() {
                    if (screen < 1000) {
                        $(".menu-topo .menu-principal").prepend("<ul class='sistema'>" + "<li class='saudacao'><a href='/no-cache/user/logout'> <span> </span> Deseja  Sair </a> </li>" + "<li class='orders'><a href='/account/orders'>Meus Pedidos</a></li>" + "<li class='account'><a href='/account'>Minha Conta</a></li>" + "</ul>")
                    }
                }, 500)
            } else {
                $(".header .saudacao-topo").prepend('<ul>' + '<li><a href="/login">login</a></li>' + '<li><a href="/login">Cadastre-se</a></li>');
                setTimeout(function() {
                    if (screen < 1000) {
                        $(".menu-topo .menu-principal").prepend("<ul class='sistema'>" + "<li class='saudacao'><a href='/login'> <span> </span> Bem-Vindo!  Entrar </a> </li>" + "<li class='orders'><a href='/account/orders'>Meus Pedidos</a></li>" + "<li class='login'><a href='/account'>Meu Cadastro</a></li>" + "</ul>")
                    }
                }, 500)
            }
            $(".saudacao-icon").click(function() {
                $('.header .saudacao-topo ul').fadeToggle()
            })
        })
    },
    slidePrincipal: function() {
        if ($("body").hasClass("home")) {
            if(screen > 1000){
                $('.slide').slick();
            }else{
                $('.slide-mobile').slick();
            }
        }
    },
    carrosselMarcas: function() {
        if ($("body").hasClass("home")) {
            $('.carrossel-marcas .marcas-carrossel ul').slick({
                infinite: !0,
                slidesToShow: 4,
                slidesToScroll: 4,
                variableWidth: !0,
                responsive: [{
                    breakpoint: 1210,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: !0
                    }
                }, {
                    breakpoint: 500,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: !0
                    }
                }]
            })
        }
    },
    instaFeed: function() {
        if ($('body').hasClass('dp-home')) {
            let userFeed = new Instafeed({
                get: 'user',
                userId: '2009865426',
                accessToken: '2009865426.1677ed0.26df4247ef97481486ba4c976560e610',
                resolution: 'low_resolution',
                template: '<a href="{{link}}" target="_blank" id="{{id}}"><img src="{{image}}" /> <div class="infos"><span class="likes"> {{likes}} </span> <span class="comentario"> {{comments}} </span></div></a>',
                sortBy: 'most-recent',
                limit: 12,
                links: !1,
                after: function() {
                    $("#instafeed").slick({
                        infinite: !0,
                        slidesToShow: 5,
                        centerMode: !0,
                        autoplay: !0,
                        autoplaySpeed: 5000,
                        variableWidth: !0
                    });
                    $(".container.insta").fadeIn()
                },
            });
            userFeed.run()
        }
    },
    slidePrateleira: function() {
        $('.slide-prod').each(function() {
            let slide = $(this).find('.dp-prateleira').length;
            if (slide > 0) {
                $(this).find('.dp-prateleira > ul').slick({
                    infinite: !0,
                    slidesToShow: 4,
                    slidesToScroll: 4,
                    responsive: [{
                        breakpoint: 1290,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3,
                            infinite: !0
                        }
                    }, {
                        breakpoint: 999,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2,
                            dots: !0,
                            infinite: !0
                        }
                    }, {
                        breakpoint: 500,
                        settings: {
                            centerPadding: '60px',
                            slidesToShow: 2,
                            slidesToScroll: 2,
                            dots: !0,
                            infinite: !0
                        }
                    }]
                })
            } else {
                $(this).hide()
            }
        })
    },
    descontoPrat: function() {
        $(".priceLabel").each(function() {
            let valor = $(this).html();
            valor = valor.replace(" %", "").replace(",", ".").replace("<br>", "").replace("OFF", "").replace("%", "");
            valor = Number(valor);
            valor = Math.ceil(valor);
            $(this).html(valor + "%  OFF")
        })
    },
    qtdProd: function() {
        $(".qtdPrateleira .btnAcr").click(function() {
            let atual = parseInt($(this).prev(".qtdVal").text());
            atual = atual + 1;
            $(this).prev(".qtdVal").text(atual)
        });
        $(".qtdPrateleira .btnDec").click(function() {
            let atual = parseInt($(this).next(".qtdVal").text());
            if (atual == 1) {
                $(this).next(".qtdVal").text("1")
            } else {
                atual = atual - 1;
                $(this).next(".qtdVal").text(atual)
            }
        });
        $(".dp-prateleira ul li .data .add a, .produto-all #produtoDivs-right .produto-direito-02 .buy-button").click(function() {
            event.preventDefault();
            let hrefCart = $(this).attr("href");
            let qtd = $(this).parent().parent().parent().find(".qtdPrateleira .qtdVal").text();
            let res = hrefCart.replace("qty=1", "qty=" + qtd);
            window.location = res
        })
    },
    breadCrumb: function() {
        $(".breadCrumb ul li").first().find("a").text("Home");
        if ($("body").hasClass("dp-404")) {
            $(".sistema .breadCrumb ul li").after('<li> 404 </li>')
        } else if ($("body").hasClass("dp-500")) {
            $(".sistema .breadCrumb ul li").after('<li> 500 </li>')
        } else if ($("body").hasClass("dp-buscavazia")) {
            $(".sistema .breadCrumb ul li").after('<li> Busca Vazia </li>')
        } else if ($("body").hasClass("resultado-busca")) {
            $(".breadCrumb ul li").after('<li> Resultado de busca </li>')
        }
    },
    bannerDepartamento: function() {
        if ($("body").hasClass("dp-categoria")) {
            let caminhoImg = $(".bannerDepartamento img").attr("src");
            if (caminhoImg) {
                $(".departamentoTopo").css("background", "url('" + caminhoImg + ") center center no-repeat")
            }
        }
    },
    filtroCategoria: function() {
        if ($("body").hasClass("dp-categoria")) {
            $(".filtrosCategoria .search-single-navigator h5, .filtrosCategoria .search-single-navigator h4").each(function(e) {
                let qtdFiltros = $(this).next('ul').find('li').length;
                if (qtdFiltros < 1) {
                    $(this).next('ul').remove();
                    $(this).remove()
                }
            });
            $(".filtrosCategoria .search-single-navigator h5, .filtrosCategoria .search-single-navigator h4").click(function(e) {
                e.preventDefault();
                $(this).next("ul").slideToggle();
                $(this).toggleClass("aberto")
            });
            $(".filtroCategoria .filtrosCategoria .navigation-tabs .menu-departamento .search-single-navigator fieldset label input").click(function() {
                $(".filtroCategoria .filtrosCategoria .navigation-tabs .menu-departamento .search-multiple-navigator .bt-refinar").click()
            });
            $(".resultadoCategoria .sub").first().find('.resultado-busca-filtro').append("<div class='exibicao-cat'><span class='col-tres check'> </span> <span class='col-quatro'> </span></div>");
            $(".resultadoCategoria .exibicao-cat .col-quatro").click(function() {
                if (!$(this).hasClass("check")) {
                    $(".resultadoCategoria .exibicao-cat .col-tres").removeClass("check");
                    $(".resultadoCategoria .exibicao-cat .col-quatro").toggleClass("check");
                    $(".dp-prateleira > ul > li").addClass("quatro-col")
                }
            });
            $(".resultadoCategoria .exibicao-cat .col-tres").click(function() {
                if (!$(this).hasClass("check")) {
                    $(".resultadoCategoria .exibicao-cat .col-quatro").removeClass("check");
                    $(".resultadoCategoria .exibicao-cat .col-tres").toggleClass("check");
                    $(".dp-prateleira > ul > li").removeClass("quatro-col")
                }
            })
        } else if ($("body").hasClass("resultado-busca")) {
            $(".resultado-busca .search-single-navigator h3").click(function(e) {
                e.preventDefault();
                $(this).next("ul").slideToggle();
                $(this).toggleClass("aberto")
            })
        }
    },
    descricaoCategoria: function() {
        if ($("body").hasClass("dp-categoria")) {
            let contDesc = $(".descricao-catgoria .conteudo-descricao").html();
            if (contDesc.length < 1) {
                $(".descricao-catgoria").remove()
            }
        }
    },
    scrollInfinito: function() {
        if ($("body").hasClass("dp-categoria")) {
            $(".dp-prateleira").addClass("prateleira");
            $(".prateleira[id*=ResultItems]").QD_infinityScroll({
                callback: function() {
                    codeEplusAlpha.descontoPrat();
                }
            });
            $(window).bind("QuatroDigital.is_noMoreResults", function() {
                $(".loadProd").after("<div class='noResults'><p>Não existem mais resultados</p></div>");
                $(".loadProd").hide()
            })
        }
    },
    imgProd: function() {
        $("#produtoDivs-left .img-produto .thumbs").slick({
            slidesToShow: 6,
            slidesToScroll: 6
        })
        $(window).load(function() {   
            if ($("body").hasClass("produto")) {
                window.LoadZoom = function(pi) {
                    let opcoesZomm = {
                        zoomWidth: 680,
                        zoomHeight: 475,
                        preloadText: ''
                    };
                    if (screen > 769) {
                        $(".image-zoom").jqzoom(opcoesZomm)
                    }
                }
                LoadZoom(0);
                ImageControl($("ul.thumbs a:first"), 0)
            }
        })
    },
    parcProd: function() {
        $(".valor-parcelado .titulo-parcelamento").text("Veja nossas opções de parcelamento");
        $(".valor-parcelado .titulo-parcelamento").click(function() {
            $(".valor-parcelado .other-payment-method-ul").slideToggle();
            $(this).toggleClass("aberto")
        })
    },
    acessoriosProd: function() {
        if ($("body").hasClass("dp-produto")) {
            let imgProd = $('.produto-all #produtoDivs-left .apresentacao #show #include #image-main, .produto-all #produtoDivs-left .apresentacao #show #include #image #image-main').attr('src');
            let nomeProd = $('.nome-prod .productName').text();
            let valorProd = $("#produtoDivs-right .plugin-preco .descricao-preco .valor-a-vista-novo b").text();
            let valProdParc = $("#produtoDivs-right .plugin-preco .descricao-preco .valor-dividido-novo").text();
            let qtdProdCJ = $(".compreJunto .dp-compre-junto > ul > li").length;
            if (qtdProdCJ == 2) {
                $('.compreJunto .prod-page .img-prod').prepend('<img src=" ' + imgProd + ' " alt="Imagem do produto">');
                $('.compreJunto .prod-page .nome-prod').prepend(nomeProd);
                $('.compreJunto .prod-page .price-prod').prepend(valorProd);
                $('.compreJunto .prod-page .price-parc-prod').prepend(valProdParc);
                let bestPrice = valorProd;
                bestPrice = bestPrice.replace("R$", "").replace(",", ".");
                bestPrice = parseInt(bestPrice);
                bestPrice = bestPrice - bestPrice / 10 / 2;
                bestPrice = bestPrice.toFixed(2);
                bestPrice = bestPrice.replace(".", ",");
                $(".compreJunto .prod-page .price-bol-prod").text("Ou R$" + bestPrice + "* para boleto.");
                $('.compreJunto .box-preco-atualizado dt').last().html('Comprar <span> 1 </span> iten(s) por');
                $('.compreJunto fieldset').click(function() {
                    let qtdProd = $('.compreJunto .box-preco-atualizado dl dd.selected-count').text();
                    $('.compreJunto .box-preco-atualizado dt').last().find('span').text(qtdProd);
                    $(this).find("input").toggleClass("check")
                });
                setTimeout(valorBoleto2,1000);

            } else {
                $(".compreJunto").hide()
            }
        }
    },
    carrosselProd: function() {
        if ($("body").hasClass("produto")) {
            let qtdCJ = $("#compre-junto-produto #divCompreJunto").html().length;
            let qtdQVVT = $("#quem-viu .dp-prateleira ul li").length;
            if (qtdCJ < 1) {
                $("#compre-junto-produto").remove()
            }
            if (qtdQVVT < 1) {
                $("#compre-junto-produto").remove()
            }
            $("#compre-junto-produto #divCompreJunto .buy").prepend("<h3> Compre <b>Junto</b> </h3>")
        }
    },
    institucional: function() {
        if (screen < 1000) {
            $(".menu-lateral-institucional h3").click(function() {
                $(this).toggleClass("aberto");
                $(".menu-lateral-institucional ul").slideToggle()
            })
        }
    },
    menuMobile: function() {
        if (screen < 1000) {
            $(".btn-menu-mobile").fadeToggle();
            setTimeout(function() {
                $(".btn-menu-mobile").fadeToggle();
                $(".btn-menu-mobile").click(function() {
                    $("body").toggleClass("aberto")
                });
                $(".menu-topo").after("<span class='menu-fechar'> </span>");
                $(".menu-fechar").click(function() {
                    $("body").toggleClass("aberto")
                })
            }, 2500);
            setTimeout(function() {
                $(".menu-topo .menu-principal > li.subMenuTrue").each(function() {
                    let linkTudo = $(this).find("a").attr("href");
                    $(this).find("ul .container-center").append("<li class='verTudo subItem'><a href='" + linkTudo + "'>Ver Tudo</a></li>")
                });
                $(".menu-topo .menu-principal > li.subMenuTrue > a").click(function(e) {
                    e.preventDefault()
                });
                $(".menu-principal > li > ul li ul").each(function(i) {
                    let subMenu = $(this).find(".subItemSub").length;
                    if (subMenu < 1) {
                        $(this).remove()
                    } else {
                        $(this).closest("li").addClass("subMenuTrue")
                    }
                });
                $(".menu-topo .menu-principal > li.subMenuTrue > a").click(function() {
                    $(this).parent().find("ul").first().slideToggle();
                    $(this).parent().toggleClass("aberto")
                });
                $(".menu-topo .menu-principal > li.subMenuTrue .subMenuTrue > a").click(function(e) {
                    e.preventDefault()
                });
                $(".menu-topo .menu-principal > li.subMenuTrue .subMenuTrue").click(function() {
                    $(this).find("ul").first().slideToggle();
                    $(this).toggleClass("aberto")
                })
            }, 4000)
        }
    },
    informativoMobile: function() {
        if (screen < 1000) {
            $('.informativo ul').slick({
                infinite: !0,
                slidesToShow: 2,
                slidesToScroll: 2,
                dots: !0
            })
        }
    },
    footerMobile: function() {
        if (screen < 1000) {
            $(".footer-01 .footer-01-02 h3, .footer-01 .footer-01-03 h3, .footer-01 .footer-01-04 h3").click(function() {
                $(this).next("ul").slideToggle();
                $(this).toggleClass("aberto")
            });
            $(".footer-01 .footer-01-01 h3").click(function() {
                $(this).parent().find("p").slideToggle();
                $(this).toggleClass("aberto")
            });
            $(".footer-02 .footer-02-01 h3, .footer-02 .footer-02-02 h3, .footer-02 .footer-02-03 h3, .footer-02 .footer-02-04 h3").click(function() {
                $(this).parent().find("span").slideToggle();
                $(this).toggleClass("aberto")
            });
            $(".footer-01 .footer-01-01 h3").click(function() {
                $(this).parent().find("p").slideToggle();
                $(this).toggleClass("aberto")
            })
        }
    },
    filtroMobile: function() {
        if (screen < 1000) {
            var qtdFiltro = $('.filtrosCategoria .search-single-navigator ul').length;
            if (qtdFiltro > 0) {
                $('.filtrosCategoria .search-single-navigator').hide();
                $('.filtrosCategoria .title-filtro').click(function() {
                    $(this).toggleClass('aberto');
                    $('.filtrosCategoria .search-single-navigator').slideToggle()
                })
            } else {}
        }
    },

    init: function() {
        /*codeEplusAlpha.menuPrincial();
        codeEplusAlpha.qtdCart();
        codeEplusAlpha.verificaLogado();
        codeEplusAlpha.slidePrincipal();
        codeEplusAlpha.carrosselMarcas();
        codeEplusAlpha.instaFeed();
        codeEplusAlpha.slidePrateleira();
        codeEplusAlpha.descontoPrat();
        codeEplusAlpha.qtdProd();
        codeEplusAlpha.breadCrumb();
        codeEplusAlpha.bannerDepartamento();
        codeEplusAlpha.filtroCategoria();
        codeEplusAlpha.descricaoCategoria();
        codeEplusAlpha.scrollInfinito();
        codeEplusAlpha.imgProd();
        codeEplusAlpha.parcProd();
        codeEplusAlpha.acessoriosProd();
        codeEplusAlpha.carrosselProd();
        codeEplusAlpha.institucional();
        codeEplusAlpha.menuMobile();
        codeEplusAlpha.informativoMobile();
        codeEplusAlpha.footerMobile();
        codeEplusAlpha.filtroMobile();*/
    }
}
codeEplusAlpha.init()